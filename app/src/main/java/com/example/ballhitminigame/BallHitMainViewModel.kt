package com.example.ballhitminigame

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ballhitminigame.ui.screens.Screens
import com.example.ballhitminigame.util.*
import com.onesignal.OneSignal
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class BallHitMainViewModel: ViewModel() {
    private var request: Job? = null
    private var topAssetDownload: Job? = null
    private var bottomAssetDownload: Job? = null

    // Public

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    fun retryDownload() {
        Navigator.navigateTo(Screens.SPLASH_SCREEN)
        downloadAssets()
    }

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = BallHitServerClient.create().getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> downloadAssets()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                downloadAssets()
                            }
                            else -> viewModelScope.launch {
                                BallHitAppData.url = "https://${splash.body()!!.url}"
                                Navigator.navigateTo(Screens.WEB_VIEW)
                            }
                        }
                    } else downloadAssets()
                } else downloadAssets()
            }
        } catch (e: Exception) {
            downloadAssets()
        }
    }

    private lateinit var tarTop: Target
    private lateinit var tarBottom: Target
    private fun downloadAssets() {
        if(topAssetDownload?.isActive == true) {
            topAssetDownload?.cancel()
        }
        topAssetDownload = viewModelScope.launch {
            val picasso = Picasso.get()

            tarTop = object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                    switchToErrorScreen()
                }
                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    if(bitmap != null) {
                        BallHitAppData.playerBitmapTop = bitmap
                        if(BallHitAppData.checkAssets()) switchToGame()
                    } else {
                        switchToErrorScreen()
                    }
                }
            }
            picasso.load(UrlTop).into(tarTop)
        }
        if(bottomAssetDownload?.isActive == true) {
            bottomAssetDownload?.cancel()
        }
        bottomAssetDownload = viewModelScope.launch {
            val picasso = Picasso.get()

            tarBottom = object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                    switchToErrorScreen()
                }
                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    if(bitmap != null) {
                        BallHitAppData.playerBitmapBottom = bitmap
                        if(BallHitAppData.checkAssets()) switchToGame()
                    } else {
                        switchToErrorScreen()
                    }
                }
            }
            picasso.load(UrlBottom).into(tarBottom)
        }
    }

    private fun switchToGame() {
        Navigator.navigateTo(Screens.GAME_SCREEN)
    }

    private fun switchToErrorScreen() {
        Navigator.navigateTo(Screens.ASSETS_LOADING_ERROR_SCREEN)
    }
}