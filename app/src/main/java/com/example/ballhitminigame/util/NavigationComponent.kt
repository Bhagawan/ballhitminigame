package com.example.ballhitminigame.util

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.ballhitminigame.ui.screens.BallHitErrorScreen
import com.example.ballhitminigame.ui.screens.BallHitSplashScreen
import com.example.ballhitminigame.ui.screens.BallHitWebViewScreen
import com.example.ballhitminigame.ui.screens.Screens
import com.example.ballhitminigame.ui.screens.gamescreen.BallHitGameScreen
import im.delight.android.webview.AdvancedWebView
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, webView: AdvancedWebView, errorScreenAction : () -> Unit, orientationChange: () -> Unit) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            BallHitSplashScreen()
        }
        composable(Screens.ASSETS_LOADING_ERROR_SCREEN.label) {
            BackHandler(true) {}
            BallHitErrorScreen(errorScreenAction)
        }
        composable(Screens.WEB_VIEW.label) {
            BackHandler(true) {
                if (webView.onBackPressed()) exitProcess(0)
            }
            BallHitWebViewScreen(webView, remember { BallHitAppData.url })
        }
        composable(Screens.GAME_SCREEN.label) {
            BackHandler(true) {}
            BallHitGameScreen()
        }
    }
    val currentScreen by Navigator.navigationFlow.collectAsState(initial = Screens.SPLASH_SCREEN)
    if(currentScreen == Screens.GAME_SCREEN) orientationChange()
    navController.navigate(currentScreen.label)

    navController.enableOnBackPressed(true)
}