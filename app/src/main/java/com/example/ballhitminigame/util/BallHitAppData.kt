package com.example.ballhitminigame.util

import android.graphics.Bitmap

object BallHitAppData {
    var url = ""
    var playerBitmapTop :Bitmap? = null
    var playerBitmapBottom :Bitmap? = null

    fun checkAssets(): Boolean = playerBitmapTop != null && playerBitmapBottom != null
}