package com.example.ballhitminigame.util

import androidx.annotation.Keep

@Keep
data class BallHitSplashResponse(val url : String)