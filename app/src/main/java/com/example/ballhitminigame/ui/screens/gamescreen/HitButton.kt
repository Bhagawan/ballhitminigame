package com.example.ballhitminigame.ui.screens.gamescreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import com.example.ballhitminigame.R
import com.example.ballhitminigame.ui.theme.Green
import com.example.ballhitminigame.ui.theme.Green_light

@Preview
@Composable
fun HitButton(onClick: () -> Unit = {}) {
    val brush = remember { Brush.radialGradient(listOf(Green, Green_light)) }
    Button(onClick = onClick, shape = CircleShape, elevation = ButtonDefaults.elevation(defaultElevation = Dp(0.0f), pressedElevation = Dp(0.0f)),
        colors = ButtonDefaults.buttonColors(Color.Transparent),
        modifier = Modifier
            .fillMaxSize()
            .shadow(Dp(2.0f), CircleShape)
            .background(brush = brush, shape = CircleShape)
            .aspectRatio(1.0f)) {
        Box(modifier = Modifier
            .fillMaxSize()
            .aspectRatio(1.0f)){
            Image(painterResource(id = R.drawable.ic_aim), stringResource(id = R.string.desc_hit),
                modifier = Modifier
                    .padding(Dp(10.0f))
                    .fillMaxSize(), contentScale = ContentScale.Fit)
        }
    }
}