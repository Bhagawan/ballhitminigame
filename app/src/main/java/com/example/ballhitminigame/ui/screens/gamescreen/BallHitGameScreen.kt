package com.example.ballhitminigame.ui.screens.gamescreen

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.ballhitminigame.ui.theme.Grey
import com.example.ballhitminigame.ui.theme.Red
import com.example.ballhitminigame.ui.theme.Yellow_light
import com.example.ballhitminigame.view.BallHitGameView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Preview
@Composable
fun BallHitGameScreen() {
    val viewModel = viewModel<BallHitViewModel>()
    val score by viewModel.scoreFlow.collectAsState()
    Row(modifier = Modifier.fillMaxSize().background(color = Yellow_light)) {
        Box(modifier = Modifier.weight(1.0f, true)) {
            HitPanel(viewModel::pressTopLeft, viewModel::pressBottomLeft)
        }
        Box(modifier = Modifier
            .weight(4.0f, true)) {
            AndroidView(factory = { BallHitGameView(it) },
                modifier = Modifier.fillMaxSize().background(color = Grey, shape = RoundedCornerShape(Dp(15.0f)))
                    .border(width = Dp(5.0f), color = Red, shape = RoundedCornerShape(Dp(15.0f))),
                update = {
                    viewModel.stanceState.onEach { stance -> it.setStance(stance) }.launchIn(viewModel.viewModelScope)
                    viewModel.restartFlow.onEach { _-> it.restart() }.launchIn(viewModel.viewModelScope)
                    it.setInterface(object: BallHitGameView.BallHitInterface {
                        override fun onEnd() {
                            viewModel.end()
                        }
                        override fun livesLeft(amount: Int) {
                            viewModel.setLives(amount)
                        }
                        override fun points(amount: Int) {
                            viewModel.setScore(amount)
                        }
                    })
                })
        }
        Box(modifier = Modifier.weight(1.0f, true)) {
            HitPanel(viewModel::pressTopRight, viewModel::pressBottomRight)
        }
    }

    val introPopup = viewModel.introState.collectAsState(true)
    val endPopup = viewModel.endPopupState.collectAsState(false)

    if(introPopup.value) BallHitIntroPopup(viewModel::restart)
    else if( endPopup.value) BallHitEndPopup(score, viewModel::restart)
}