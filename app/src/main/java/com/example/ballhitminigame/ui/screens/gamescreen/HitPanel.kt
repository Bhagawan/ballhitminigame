package com.example.ballhitminigame.ui.screens.gamescreen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview

@Preview
@Composable
fun HitPanel(topButtonAction: ()-> Unit = {}, bottomButtonAction: ()-> Unit = {}) {
    Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.SpaceBetween) {
        Box(modifier = Modifier.weight(1.0f, true)) {
            HitButton(topButtonAction)
        }
        Box(modifier = Modifier.weight(1.0f, true)) {
            HitButton(bottomButtonAction)
        }
    }
}