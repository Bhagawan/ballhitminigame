package com.example.ballhitminigame.ui.screens.gamescreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ballhitminigame.view.PlayerStance
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class BallHitViewModel: ViewModel() {
    private val _stanceState = MutableStateFlow(PlayerStance.TOP_LEFT)
    val stanceState = _stanceState.asStateFlow()

    private val _introState = MutableStateFlow(true)
    val introState = _introState.asStateFlow()

    private val _endPopupState = MutableStateFlow(false)
    val endPopupState = _endPopupState.asStateFlow()

    private val _restartFlow = MutableSharedFlow<Boolean>()
    val restartFlow = _restartFlow.asSharedFlow()

    private val _scoreFlow = MutableStateFlow(0)
    val scoreFlow = _scoreFlow.asStateFlow()

    private val _livesFlow = MutableStateFlow(5)
    val livesFlow = _livesFlow.asStateFlow()

    /// Public

    fun pressTopLeft() {
        _stanceState.tryEmit(PlayerStance.TOP_LEFT)
    }

    fun pressTopRight() {
        _stanceState.tryEmit(PlayerStance.TOP_RIGHT)
    }

    fun pressBottomLeft() {
        _stanceState.tryEmit(PlayerStance.BOTTOM_LEFT)
    }

    fun pressBottomRight() {
        _stanceState.tryEmit(PlayerStance.BOTTOM_RIGHT)
    }

    fun end() {
        _endPopupState.tryEmit(true)
    }

    fun setScore(score: Int) {
        _scoreFlow.tryEmit(score)
    }

    fun setLives(amount: Int) {
        _livesFlow.tryEmit(amount)
    }

    fun restart() {
        if(introState.value) _introState.tryEmit(false)
        _endPopupState.tryEmit(false)
        viewModelScope.launch {
            _restartFlow.emit(true)
        }
    }
}