package com.example.ballhitminigame.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Green_light = Color(0xFF9FB685)
val Grey_light = Color(0xFF6B6B6B)
val Grey = Color(0xFF4B4B4B)
val Green = Color(0xFF597E2E)
val Red = Color(0xFFA72E2E)
val Gold = Color(0xFFE6A23F)
val Grey_Transparent = Color(0x803A3A3A)
val Brown = Color(0xFF3C1F00)
val Yellow_light = Color(0xFFA59B7C)