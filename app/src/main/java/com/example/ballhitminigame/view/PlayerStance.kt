package com.example.ballhitminigame.view

enum class PlayerStance {
    TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT
}