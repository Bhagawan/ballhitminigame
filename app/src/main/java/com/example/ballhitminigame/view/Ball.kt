package com.example.ballhitminigame.view

data class Ball(var x: Float,
                var y: Float,
                val position: PlayerStance,
                var angle : Int = 0,
                var reflectDX: Float? = null,
                var reflectDY: Float? = null)
