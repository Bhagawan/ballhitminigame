package com.example.ballhitminigame.view

import android.content.Context
import android.graphics.*
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import com.example.ballhitminigame.R
import com.example.ballhitminigame.util.BallHitAppData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.random.Random


class BallHitGameView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var ballRadius = 1.0f
    private var ballSpeedY = 1.0f
    private var ballSpeedX = 1.0f

    private var ballSpawnTimer = 0
    private var ballSpawnInterval = 60.0f

    private var livesLeft = 5
    private var points = 0

    companion object {
        const val STATE_GAME = 1
        const val STATE_PAUSE = 2
    }

    private var ballBitmap = AppCompatResources.getDrawable(context, R.drawable.ic_football)?.toBitmap()
    private var playerTopLeftBitmap: Bitmap? = null
    private var playerTopRightBitmap: Bitmap? = null
    private var playerBottomLeftBitmap: Bitmap? = null
    private var playerBottomRightBitmap: Bitmap? = null

    private val stands = ArrayList<Path>()
    private val balls = ArrayList<Ball>()

    private var state = STATE_PAUSE
    private var playerStance = PlayerStance.TOP_LEFT

    private var mInterface: BallHitInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            ballRadius = mHeight / 15.0f
            ballSpeedX = mWidth * 0.2f * 0.96f / 60
            ballSpeedY = mHeight * 0.2f / 60.0f
            createPlayerBitmaps()
            createStands()
            setBallSize()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            drawField(it)
            drawPlayer(it)
            if(state == STATE_GAME) {
                updateBalls()
                updateInterface()
            }
            drawBalls(it)
            drawUI(it)
        }
    }

    /// Public

    fun setInterface(i: BallHitInterface) {
        mInterface = i
    }

    fun restart() {
        ballSpawnInterval = 60.0f
        ballSpawnTimer = ballSpawnInterval.toInt()
        balls.clear()
        livesLeft = 5
        points = 0
        state = STATE_GAME
    }

    fun setStance(stance: PlayerStance) {
        playerStance = stance
    }

    //// Private

    private fun drawField(c: Canvas) {
        val p = Paint()
        p.color = Color.argb(255,60,31,0)
        for(stand in stands) c.drawPath(stand, p)
    }

    private fun drawBalls(c: Canvas) {
        val p = Paint()
        for(ball in balls) ballBitmap?.let {
            val matrix = Matrix()
            matrix.postRotate(-ball.angle.toFloat())
            val rotatedBall = Bitmap.createBitmap(it, 0, 0, it.width, it.height, matrix, true)
            c.drawBitmap(rotatedBall, null, Rect((ball.x - rotatedBall.width / 2).toInt(), (ball.y - rotatedBall.height / 2).toInt(), (ball.x + rotatedBall.width / 2).toInt(), (ball.y + rotatedBall.height / 2).toInt()), p)
        }
    }

    private fun drawPlayer(c: Canvas) {
        when(playerStance) {
            PlayerStance.BOTTOM_RIGHT -> playerBottomRightBitmap?.let { c.drawBitmap(it, mWidth * 0.8f - it.width, mHeight - it.height.toFloat() , Paint()) }
            PlayerStance.BOTTOM_LEFT -> playerBottomLeftBitmap?.let { c.drawBitmap(it,mWidth * 0.2f,mHeight - it.height.toFloat() , Paint()) }
            PlayerStance.TOP_LEFT -> playerTopLeftBitmap?.let { c.drawBitmap(it, mWidth * 0.2f, mHeight * 0.4f , Paint()) }
            PlayerStance.TOP_RIGHT -> playerTopRightBitmap?.let { c.drawBitmap(it, mWidth * 0.8f - it.width, mHeight * 0.4f , Paint()) }
        }
    }

    private fun drawUI(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.textSize = 60.0f
        p.isFakeBoldText = true

        val ballSize = mWidth / 15
        ballBitmap?.let { for(n in 0 until livesLeft)  c.drawBitmap(it, null, Rect(15 + n * (ballSize + 15), 15, 15 + n * (ballSize + 15) + ballSize, 15 + ballSize), p) }

        c.drawText(points.toString(), mWidth / 3.0f * 2, 70.0f, p)
    }

    private fun updateBalls() {
        val standWidth = mWidth * 0.2f
        for(ball in balls) {
            val dX: Float
            val dY: Float
            if (ball.reflectDX == null && ball.reflectDY == null) {
                when(ball.position) {
                    PlayerStance.BOTTOM_LEFT, PlayerStance.TOP_LEFT -> {
                        if(ball.x <= standWidth * 0.2f) {
                            dX = ballSpeedX.coerceAtMost( (standWidth * 0.2f - ball.x).absoluteValue)
                            dY = 0.0f
                        } else {
                            dX = ballSpeedX
                            dY = ballSpeedY
                        }
                    }
                    PlayerStance.BOTTOM_RIGHT, PlayerStance.TOP_RIGHT -> {
                        if(ball.x >= mWidth - standWidth * 0.2f) {
                            dX = -ballSpeedX.coerceAtMost( (mWidth - standWidth * 0.2f - ball.x).absoluteValue)
                            dY = 0.0f
                        } else {
                            dX = -ballSpeedX
                            dY = ballSpeedY
                        }
                    }
                }
            } else {
                dX = ball.reflectDX?: ballSpeedX
                dY = ball.reflectDY?: ballSpeedY
            }
            ball.x += dX
            ball.y += dY
            ball.angle = (ball.angle + 5 * if (ball.position == PlayerStance.TOP_LEFT || ball.position == PlayerStance.BOTTOM_LEFT) -1 else 1) % 360

            if(ball.reflectDX == null && ball.reflectDY == null) {
                when(ball.position) {
                    PlayerStance.TOP_LEFT -> {
                        if(ball.x > standWidth + ballRadius && ball.x < mWidth / 2 - ballRadius) {
                            if(playerStance == PlayerStance.TOP_LEFT) {
                                ball.reflectDY = -mHeight / 60.0f
                                ball.reflectDX = Random.nextInt((-ballRadius / 2).toInt(), (ballRadius / 2).toInt()).toFloat()
                                points++
                            }
                        } else if(ball.x > mWidth / 2 - ballRadius) {
                            ball.reflectDX = ballSpeedX
                            ball.reflectDY = ballSpeedY
                            livesLeft--
                        }
                    }
                    PlayerStance.TOP_RIGHT -> {
                        if(ball.x < mWidth - standWidth - ballRadius && ball.x > mWidth / 2 + ballRadius) {
                            if(playerStance == PlayerStance.TOP_RIGHT) {
                                ball.reflectDY = -mHeight / 60.0f
                                ball.reflectDX = Random.nextInt((-ballRadius / 2).toInt(), (ballRadius / 2).toInt()).toFloat()
                                points++
                            }
                        } else if(ball.x < mWidth / 2 + ballRadius) {
                            ball.reflectDX = -ballSpeedX
                            ball.reflectDY = ballSpeedY
                            livesLeft--
                        }
                    }
                    PlayerStance.BOTTOM_LEFT -> {
                        if(ball.x > standWidth + ballRadius && ball.x < mWidth / 2 - ballRadius) {
                            if(playerStance == PlayerStance.BOTTOM_LEFT) {
                                ball.reflectDY = -mHeight / 60.0f
                                ball.reflectDX = Random.nextInt((-ballRadius / 2).toInt(), (ballRadius / 2).toInt()).toFloat()
                                points++
                            }
                        } else if(ball.x > mWidth / 2 - ballRadius) {
                            ball.reflectDX = ballSpeedX
                            ball.reflectDY = ballSpeedY
                            livesLeft--
                        }
                    }
                    PlayerStance.BOTTOM_RIGHT -> {
                        if(ball.x < mWidth - standWidth - ballRadius && ball.x > mWidth / 2 + ballRadius) {
                            if(playerStance == PlayerStance.BOTTOM_RIGHT) {
                                ball.reflectDY = -mHeight / 60.0f
                                ball.reflectDX = Random.nextInt((-ballRadius / 2).toInt(), (ballRadius / 2).toInt()).toFloat()
                                points++
                            }
                        } else if(ball.x < mWidth / 2 + ballRadius) {
                            ball.reflectDX = -ballSpeedX
                            ball.reflectDY = ballSpeedY
                            livesLeft--
                        }
                    }
                }
            }
        }
        var n = 0
        while(n < balls.size) {
            if(balls[n].x !in -ballRadius..mWidth + ballRadius || balls[n].y !in -ballRadius..mHeight + ballRadius) {
                if(balls[n].reflectDX == null && balls[n].reflectDY == null) livesLeft--
                balls.removeAt(n)
                n--
            }
            n++
        }
        ballSpawnTimer++
        if (ballSpawnTimer > ballSpawnInterval) {
            ballSpawnTimer = 0
            spawnBall()
            ballSpawnInterval-=0.02f
        }
        if(livesLeft == 0) {
            mInterface?.onEnd()
            state = STATE_PAUSE
        }
    }

    private fun updateInterface() {
        mInterface?.let {
            it.livesLeft(livesLeft)
            it.points(points)
        }
    }

    private fun spawnBall() {
        val topLine = mHeight * 0.3f
        val bottomLine = mHeight * 0.6f
        when(val position = PlayerStance.values().random()) {
            PlayerStance.TOP_LEFT -> balls.add(Ball(ballRadius, topLine - ballRadius, position))
            PlayerStance.TOP_RIGHT -> balls.add(Ball(mWidth - ballRadius, topLine - ballRadius, position))
            PlayerStance.BOTTOM_LEFT -> balls.add(Ball(ballRadius, bottomLine - ballRadius, position))
            PlayerStance.BOTTOM_RIGHT -> balls.add(Ball(mWidth - ballRadius, bottomLine - ballRadius, position))
        }
    }

    private fun createPlayerBitmaps() {
        BallHitAppData.playerBitmapBottom?.let {
            val matrix = Matrix()
            val dX = (mWidth / 3.0f) / it.width
            matrix.postScale( dX , dX, it.width / 2f, it.height / 2f)
            playerBottomLeftBitmap = Bitmap.createBitmap(it, 0, 0, it.width, it.height, matrix, true)
        }
        BallHitAppData.playerBitmapTop?.let {
            val matrix = Matrix()
            val dX = (mWidth * 0.5f) / it.width
            matrix.postScale( dX , dX, it.width / 2f, it.height / 2f)
            playerTopRightBitmap = Bitmap.createBitmap(it, 0, 0, it.width, it.height, matrix, true)
        }
        playerBottomLeftBitmap?.let {
            val matrix = Matrix()
            matrix.postScale( -1.0f , 1.0f, it.width / 2f, it.height / 2f)
            playerBottomRightBitmap = Bitmap.createBitmap(it, 0, 0, it.width, it.height, matrix, true)
        }
        playerTopRightBitmap?.let {
            val matrix = Matrix()
            matrix.postScale( -1.0f , 1.0f, it.width / 2f, it.height / 2f)
            playerTopLeftBitmap = Bitmap.createBitmap(it, 0, 0, it.width, it.height, matrix, true)
        }
    }

    private fun setBallSize() {
        ballBitmap?.let {
            val matrix = Matrix()
            matrix.postScale(ballRadius * 2 / it.width, ballRadius * 2 / it.height)
            ballBitmap = Bitmap.createBitmap(it, 0, 0, it.width, it.height, matrix, true)
        }
    }

    private fun createStands() {
        stands.clear()
        val thickness = 15.0f
        val topLine = mHeight * 0.3f
        val bottomLine = mHeight * 0.6f
        val standHeight = mHeight * 0.2f
        val standWidth = mWidth * 0.2f
        stands.add( Path().apply {  // Top Left Stand
            moveTo(0.0f, topLine)
            lineTo(standWidth * 0.2f, topLine)
            lineTo(standWidth, topLine + standHeight)
            lineTo(standWidth, topLine + standHeight + thickness)
            lineTo(standWidth * 0.8f * 0.6f + standWidth * 0.2f, topLine + thickness + standHeight * 0.6f)
            lineTo(standWidth * 0.8f * 0.6f + standWidth * 0.2f, topLine + thickness * 5 + standHeight * 0.6f)
            lineTo(standWidth * 0.8f * 0.6f + standWidth * 0.2f - thickness, topLine + thickness * 5 + standHeight * 0.6f)
            lineTo(standWidth * 0.8f * 0.6f + standWidth * 0.2f - thickness, topLine + thickness + standHeight  * (0.6f - thickness / standHeight))
            lineTo(standWidth * 0.2f, topLine + thickness)
            lineTo(0.0f, topLine + thickness)
            close()
        })
        stands.add( Path().apply {  // Bottom Left Stand
            moveTo(0.0f, bottomLine)
            lineTo(standWidth * 0.2f, bottomLine)
            lineTo(standWidth, bottomLine + standHeight)
            lineTo(standWidth, bottomLine + standHeight + thickness)
            lineTo(standWidth * 0.8f * 0.6f + standWidth * 0.2f, bottomLine + thickness + standHeight * 0.6f)
            lineTo(standWidth * 0.8f * 0.6f + standWidth * 0.2f, bottomLine + thickness * 5 + standHeight * 0.6f)
            lineTo(standWidth * 0.8f * 0.6f + standWidth * 0.2f - thickness, bottomLine + thickness * 5 + standHeight * 0.6f)
            lineTo(standWidth * 0.8f * 0.6f + standWidth * 0.2f - thickness, bottomLine + thickness + standHeight * (0.6f - thickness / standHeight))
            lineTo(standWidth * 0.2f, bottomLine + thickness)
            lineTo(0.0f, bottomLine + thickness)
            close()
        })
        stands.add( Path().apply {  // Top Right Stand
            moveTo(mWidth.toFloat(), topLine)
            lineTo(mWidth - standWidth * 0.2f, topLine)
            lineTo(mWidth - standWidth, topLine + standHeight)
            lineTo(mWidth - standWidth, topLine + standHeight + thickness)
            lineTo(mWidth - (standWidth * 0.8f * 0.6f + standWidth * 0.2f), topLine + thickness + standHeight * 0.6f)
            lineTo(mWidth - (standWidth * 0.8f * 0.6f + standWidth * 0.2f), topLine + thickness * 5 + standHeight * 0.6f)
            lineTo(mWidth - (standWidth * 0.8f * 0.6f + standWidth * 0.2f - thickness), topLine + thickness * 5 + standHeight * 0.6f)
            lineTo(mWidth - (standWidth * 0.8f * 0.6f + standWidth * 0.2f - thickness), topLine + thickness + standHeight * (0.6f - thickness / standHeight))
            lineTo(mWidth - standWidth * 0.2f, topLine + thickness)
            lineTo(mWidth.toFloat(), topLine + thickness)
            close()
        })
        stands.add( Path().apply {  // Bottom Right Stand
            moveTo(mWidth.toFloat(), bottomLine)
            lineTo(mWidth - standWidth * 0.2f, bottomLine)
            lineTo(mWidth - standWidth, bottomLine + standHeight)
            lineTo(mWidth - standWidth, bottomLine + standHeight + thickness)
            lineTo(mWidth - (standWidth * 0.8f * 0.6f + standWidth * 0.2f), bottomLine + thickness + standHeight * 0.6f)
            lineTo(mWidth - (standWidth * 0.8f * 0.6f + standWidth * 0.2f), bottomLine + thickness * 5 + standHeight * 0.6f)
            lineTo(mWidth - (standWidth * 0.8f * 0.6f + standWidth * 0.2f - thickness), bottomLine + thickness * 5 + standHeight * 0.6f)
            lineTo(mWidth - (standWidth * 0.8f * 0.6f + standWidth * 0.2f - thickness), bottomLine + thickness + standHeight * (0.6f - thickness / standHeight))
            lineTo(mWidth - standWidth * 0.2f, bottomLine + thickness)
            lineTo(mWidth.toFloat(), bottomLine + thickness)
            close()
        })
    }

    interface BallHitInterface {
        fun onEnd()
        fun livesLeft(amount: Int)
        fun points(amount: Int)
    }
}